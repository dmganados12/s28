// Create a Schema, model for the task collection, and make sure to export.

const mongoose = require('mongoose')

const taskBlueprint = new mongoose.Schema({
	name: {
		type: String,
		require: [true, 'Task Name is Required.']
	},
	status: {
		type: String,
		default: 'Pending'
	}
});

// Create a model using the Schema
module.exports = mongoose.model("Task", taskBlueprint);