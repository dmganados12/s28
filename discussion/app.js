// [SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const taskRoutes = require('./routes/tasks')

// [SECTION] Environment Variables
	dotenv.config();
	let secret = process.env.CONNECTION_STRING;

// [SECTION] Server Setup
	const server = express();
	server.use(express.json());
	const port = process.env.PORT;


// [SECTION] Server Routes
	server.use('/tasks', taskRoutes)

// [SECTION] Database Connection
	mongoose.connect(secret);
	let db = mongoose.connection;
	db.once('open', () => console.log('Now Connected to MongoDB Atlas'));

// [SECTION] Server Response
server.listen(port, () => console.log(`Server is running on port:${port}`));

server.get('/', (req, res) => {
	res.send('Welcome to App!')
});